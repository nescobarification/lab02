package com.jcg.spring.hibernate.pojo;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;

public class Payments implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer carteCreditId, forfaitId;
	private Timestamp dateFin;
	private Boolean actif;
	
	public Integer getCarteCreditId() {
		return carteCreditId;
	}

	public void setCarteCreditId(Integer carteCreditId) {
		this.carteCreditId = carteCreditId;
	}

	public Integer getForfaitId() {
		return forfaitId;
	}

	public void setForfaitId(Integer forfaitId) {
		this.forfaitId = forfaitId;
	}

	public Timestamp getDateFin() {
		return dateFin;
	}

	public void setDateFin(Timestamp dateFin) {
		this.dateFin = dateFin;
	}

	public Boolean getActif() {
		return actif;
	}

	public void setActif(Boolean actif) {
		this.actif = actif;
	}
}