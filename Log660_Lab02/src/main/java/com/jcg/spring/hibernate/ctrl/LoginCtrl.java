package com.jcg.spring.hibernate.ctrl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.jcg.spring.hibernate.service.AuthService;

@Controller
@RequestMapping("/utilisateur")
public class LoginCtrl 
{

	@Autowired
	private AuthService authenticateService;			// Injection automatique du service.
	private static Logger log = Logger.getLogger(LoginCtrl.class);

	// Authentification de l'usager
	@RequestMapping(value = "/validate", method = RequestMethod.POST)
	public ModelAndView validateUsr(@RequestParam("username")String username, @RequestParam("password")String password) 
	{
		String msg = "";
		
		boolean isValid = authenticateService.findUser(username, password);
		
		log.info("L'usager est authentifié ?= " + isValid);

		if(isValid) {
			msg = "Bonjour " + username + "!";
		} else {
			msg = "Les Données des connexion ne sont pas valides!";
		}

		return new ModelAndView("result", "output", msg);
	}
}