package com.jcg.spring.hibernate.pojo;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;

public class FilmPersonne implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer personneId, filmId;
	private String personnage, typePersonne;
	
	public Integer getPersonneId() {
		return personneId;
	}

	public void setPersonneId(Integer personneId) {
		this.personneId = personneId;
	}

	public Integer getFilmId() {
		return filmId;
	}

	public void setFilmId(Integer filmId) {
		this.filmId = filmId;
	}

	public String getPersonnage() {
		return personnage;
	}

	public void setPersonnage(String personnage) {
		this.personnage = personnage;
	}

	public String getTypePersonne() {
		return typePersonne;
	}

	public void setTypePersonne(String typePersonne) {
		this.typePersonne = typePersonne;
	}	
}