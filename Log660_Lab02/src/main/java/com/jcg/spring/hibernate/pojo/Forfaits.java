package com.jcg.spring.hibernate.pojo;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;

public class Forfaits implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer id;
	private String typeForfait;
	private Integer cout, locationsMax, dureeMax;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTypeForfait() {
		return typeForfait;
	}

	public void setTypeForfait(String typeForfait) {
		this.typeForfait = typeForfait;
	}

	public Integer getCout() {
		return cout;
	}

	public void setCout(Integer cout) {
		this.cout = cout;
	}

	public Integer getLocationsMax() {
		return locationsMax;
	}

	public void setLocationsMax(Integer locationsMax) {
		this.locationsMax = locationsMax;
	}

	public Integer getDureeMax() {
		return dureeMax;
	}

	public void setDureeMax(Integer dureeMax) {
		this.dureeMax = dureeMax;
	}	
}