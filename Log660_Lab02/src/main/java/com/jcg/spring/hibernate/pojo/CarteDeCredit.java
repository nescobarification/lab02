package com.jcg.spring.hibernate.pojo;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;

public class CarteDeCredit implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer id;
	private Integer numero, moisExpiratoin, anneeExpiration, utilisateurId;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getNumero() {
		return numero;
	}

	public void setNumero(Integer numero) {
		this.numero = numero;
	}

	public Integer getMoisExpiratoin() {
		return moisExpiratoin;
	}

	public void setMoisExpiratoin(Integer moisExpiratoin) {
		this.moisExpiratoin = moisExpiratoin;
	}

	public Integer getAnneeExpiration() {
		return anneeExpiration;
	}

	public void setAnneeExpiration(Integer anneeExpiration) {
		this.anneeExpiration = anneeExpiration;
	}

	public Integer getUtilisateurId() {
		return utilisateurId;
	}

	public void setUtilisateurId(Integer utilisateurId) {
		this.utilisateurId = utilisateurId;
	}
}