package com.jcg.spring.hibernate.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.orm.hibernate5.HibernateTemplate; 

import com.jcg.spring.hibernate.pojo.Utilisateur;

public class AuthService {

	private HibernateTemplate hibernateTemplate;
	private Session session;
	private static Logger log = Logger.getLogger(AuthService.class);

	private AuthService() { }

	public void setHibernateTemplate(HibernateTemplate hibernateTemplate) {
		this.hibernateTemplate = hibernateTemplate;
	}
	
	public void setSessionFactory(HibernateTemplate hibernateTemplate) {
		try {
		    session = hibernateTemplate.getSessionFactory().getCurrentSession();
		} catch (HibernateException e) {
		    session = hibernateTemplate.getSessionFactory().openSession();
		}
	}

	@SuppressWarnings( { "unchecked", "deprecation" } )
	public boolean findUser(String uname, String upwd) {
		log.info("Checking if the user is in the database");
		
		boolean isValidUser = false;
		String sqlQuery = "from Utilisateur u where u.firstName = :firstName and u.password = :pwd";
		setSessionFactory(hibernateTemplate);
		
		System.out.println(upwd);
		System.out.println(uname);
		try {
			List<Utilisateur> userObj = (List<Utilisateur>) session.createQuery(sqlQuery)
					.setParameter("firstName", uname)
					.setParameter("pwd", upwd)
					.list();
			
			if(userObj != null && userObj.size() > 0) {
				log.info("Id= " + userObj.get(0).getId() + ", Name= " + userObj.get(0).getFirstName() + ", Password= " + userObj.get(0).getPassword());
				isValidUser = true;
			}
		} catch(Exception e) {
			isValidUser = false;
			log.error("An error occurred while fetching the user details from the database", e);	
		}
		return isValidUser;
	}
}