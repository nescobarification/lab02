package com.jcg.spring.hibernate.pojo;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;

public class Exemplaires implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer id;
	private Integer filmId;
	private Boolean estLoue;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getFilmId() {
		return filmId;
	}

	public void setFilmId(Integer filmId) {
		this.filmId = filmId;
	}

	public Boolean getEstLoue() {
		return estLoue;
	}

	public void setEstLoue(Boolean estLoue) {
		this.estLoue = estLoue;
	}
}