package com.jcg.spring.hibernate.pojo;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;

public class Personne implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer id;
	private String nom, lieuNaissance, biographie;
	private Timestamp dateNaissance;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getLieuNaissance() {
		return lieuNaissance;
	}

	public void setLieuNaissance(String lieuNaissance) {
		this.lieuNaissance = lieuNaissance;
	}

	public String getBiographie() {
		return biographie;
	}

	public void setBiographie(String biographie) {
		this.biographie = biographie;
	}

	public Timestamp getDateNaissance() {
		return dateNaissance;
	}

	public void setDateNaissance(Timestamp dateNaissance) {
		this.dateNaissance = dateNaissance;
	}
}