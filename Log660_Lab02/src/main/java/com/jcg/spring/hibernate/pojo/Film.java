package com.jcg.spring.hibernate.pojo;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;

public class Film implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer id;
	private Integer anneeDeSortie, langueID, duree;
	private String titre, resume;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getAnneeDeSortie() {
		return anneeDeSortie;
	}

	public void setAnneeDeSortie(Integer anneeDeSortie) {
		this.anneeDeSortie = anneeDeSortie;
	}

	public Integer getLangueID() {
		return langueID;
	}

	public void setLangueID(Integer langueID) {
		this.langueID = langueID;
	}

	public Integer getDuree() {
		return duree;
	}

	public void setDuree(Integer duree) {
		this.duree = duree;
	}

	public String getTitre() {
		return titre;
	}

	public void setTitre(String titre) {
		this.titre = titre;
	}

	public String getResume() {
		return resume;
	}

	public void setResume(String resume) {
		this.resume = resume;
	}
}