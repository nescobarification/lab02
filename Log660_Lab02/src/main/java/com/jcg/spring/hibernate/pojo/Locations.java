package com.jcg.spring.hibernate.pojo;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;

public class Locations implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer id;
	private Integer utilisateurId, exemplaireId;
	private Timestamp dateLocation, datePrevuRetour, dateRetour;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUtilisateurId() {
		return utilisateurId;
	}

	public void setUtilisateurId(Integer utilisateurId) {
		this.utilisateurId = utilisateurId;
	}

	public Integer getExemplaireId() {
		return exemplaireId;
	}

	public void setExemplaireId(Integer exemplaireId) {
		this.exemplaireId = exemplaireId;
	}

	public Timestamp getDateLocation() {
		return dateLocation;
	}

	public void setDateLocation(Timestamp dateLocation) {
		this.dateLocation = dateLocation;
	}

	public Timestamp getDatePrevuRetour() {
		return datePrevuRetour;
	}

	public void setDatePrevuRetour(Timestamp datePrevuRetour) {
		this.datePrevuRetour = datePrevuRetour;
	}

	public Timestamp getDateRetour() {
		return dateRetour;
	}

	public void setDateRetour(Timestamp dateRetour) {
		this.dateRetour = dateRetour;
	}
}