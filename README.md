Lab02


Rouler spring et lab02 sur eclipse

- Installer ou avoir JDK 8 java
- Installer plugin STS (Spring Tool Suite)
    - Mettre STS dans le search et installer le seul sur la liste.
- import
    - spring Maven project (MVC)
- Créer un serveur Tomcat v9.0
    - Right click sur le projet + New —> Other —> Server —> Apache —> Tomcat v9.0
    - Doit installer Tombât et passer l’exécutable dans le browse
    - Installation:
        - https://wolfpaulus.com/mac/tomcat/
- Pour starter le serveur avec éclipse
    - Window —> Show view —> Other —> Server
    - Dans la fenêtre d’en bas tu start le serveur.
- Pour que c fonctionne il faut installer 
- https://stackoverflow.com/questions/23570448/dynamic-web-project-option-missing-in-eclipse-kepler
In "Web, XML, Java EE and OSGi Enterprise Development", check:
1. Eclipse Java EE Developer Tools
2. Eclipse Java Web Developer Tools
3. Eclipse Web Developer Tools
4. Eclipse XML Editors and Tools
- Right click sur le projet —> Properties —> Project Facets —> Enable « Dynamic Web Module »  
- Right click sur le serveur (Fenêtre Servers) —> Add And Remove .. —> Ajouter notre projet.
- Right click sur le serveur (Fenêtre Servers) —> Start Server
- Project —> Properties —> Deployment Assembly —> Add —> JavaBuildPath Entries —> Maven Dependencies —> Finish
- Ajouter applicationContext.xml et l’autre .xml dans Spring/Bean Support
    - Right click sur projet —> Spring —> Bean Support —> Add XML Config

- Avoir une connexion a Data Source Explorer
    - https://www.youtube.com/watch?v=2orrUVCrrJo

- Install OJDBC6:
    - mvn install:install-file -Dfile=ojdbc6.jar -DartifactId=ojdbc6 -Dversion=11.2.0 -Dpackaging=jar -DgroupId=com.oracle
    - https://stackoverflow.com/questions/25128219/missing-artifact-com-oracleojdbc6jar11-2-0-in-pom-xml


- Mysql psw rootroot